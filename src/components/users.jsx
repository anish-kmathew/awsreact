import React, { Component } from "react";
import { toast } from "react-toastify";


import "../HoverButton.css";
import { getUsers, deleteUser } from "../services/userService";

//const imgPath = process.env.REACT_APP_AWS_BUCKET+'/';


class Users extends Component {
  state = {
    users: [],
    loader: 1
  };

  async componentDidMount() {
    const { data: users } = await getUsers();
    this.setState({ users: users.data, loader: 0 });
  }

  handleDelete = async user => {
    const originalUsers = this.state.users;
    const users = originalUsers.filter(u => u.id !== user.id);
    this.setState({ users });

    try {
      await deleteUser(user.id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404) console.log("x");
      toast.error("This user has already been deleted.");

      this.setState({ users: originalUsers });
    }
  };

  render() {
    const { users } = this.state;
    if (this.state.loader === 1)
      return (
        <div
          className="row justify-content-center"
        >
          <img alt="Loader" src="giphy.gif" />
        </div>
      );
    else
      return (
        <div
          className="row justify-content-center" >
          <div className="col-md-10">            
            <div className="row  ">
              {users.map(user => (
                <div className="col-md-12" key={user.id}>                  
                  <h5>{user.id}. {user.name}</h5>
                  <h6>Email : {user.email}</h6>
                </div>
              ))}
            </div>
          </div>
        </div>
      );
  }
}

export default Users;
